// https://www.youtube.com/watch?v=lSrBOXYusUs&t=1358s
// //type definition
// //string
// //number
// //any
// //boolean
// //array
// //object
// //truple
// //enum
// //null
// //undefined
// var firstName:string = "Shorif";
// var age:number = 20;
// var web_developer:boolean = true;
// // var jobs:any[] = ['web_developer', 'programmer', 10];
// // var jobs:Array<any> = ['web_developer', 'programmer', 10];
// //array
// var jobs:[string, string, number, boolean];
// jobs = ['web_developer', 'programmer', 10, true];
// console.log(jobs[0]);
// //enum
// enum Color {Red, Green, Blue}; //variable name er prothom okkhor capital hobe.
// enum Color1 {Red = 0, Green = 1, Blue = 2}; //variable name er prothom okkhor capital hobe.
// var chosenColor = Color.Green; 
// console.log(chosenColor);
// var enginner:null = null;
// var enginner1:undefined = undefined;
// function bio(self:object):void { //jodi return na kori tahole void use korte hobe && jodi return kori tahole object return korbe.
//     console.log(self);
//     // return self;
// }
// bio({
//     firstName: 'shorif',
//     lastName: 'ahmed',
//     age: 28
// })
// function bio(firstName:string, age:number):void{
//     // console.log('my name is ' + firstName + ' and i am ' + age)
//     console.log(`My name is ${firstName} and i am ${age}`)
// }
// bio('shorif', 28)
// function printNumber(number:number):void{
//     for(var i = 0; i < number; i++){
//         console.log(i);
//     }
//     console.log(i);
// }
// printNumber(10);
// //Arrow function
// let bio = (firstName, age)=>{
//     console.log(`My name is ${firstName} and i am ${age}`);
// }
// bio('shorif', 29)
// //Interface
// function bio(self:Self):void { //jodi return na kori tahole void use korte hobe && jodi return kori tahole object return korbe.
//     console.log(`My name is ${self.firstName} ${self.lastName} and i am ${self.age}`)
//     // return self;
// }
// bio({
//     firstName: 'shorif',
//     lastName: 'ahmed',
//     age: 28
// })
// interface Self{
//     firstName: string;
//     lastName: string;
//     age: number;
// }
//Class
var Person = /** @class */ (function () {
    function Person(firstName, lastName, age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
    Person.prototype.showBio = function () {
        console.log("My name is ".concat(this.firstName, " ").concat(this.lastName, " and I am ").concat(this.age));
    };
    return Person;
}());
var mySelf = new Person('shorif', 'ahmed', 28);
mySelf.showBio();
var Person1 = /** @class */ (function () {
    function Person1(firstName, lastName, age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
    Person1.prototype.showBio1 = function () {
        console.log("My name is ".concat(this.firstName, " ").concat(this.lastName, " and I am ").concat(this.age));
    };
    Person1.prototype.getBio1 = function () {
        return this.showBio1();
    };
    Person1.prototype.setBio = function (firstName, lastName, age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    };
    return Person1;
}());
var mySelf1 = new Person1('shorif', 'ahmed', 29);
mySelf1.getBio1();
mySelf1.setBio('shojib', 'khan', 19);
mySelf1.getBio1();
